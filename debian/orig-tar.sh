#!/bin/sh -e

# called by uscan with '--upstream-version' <version> <file>
DIR=springpython-$2+ds
TAR=springpython_$2+ds.orig.tar.gz

# Repack upstream source to tar.gz and clean it
tar zxf $3
mv springpython-* $DIR
GZIP=--best tar -cz --owner root --group root --mode a+rX -f $TAR $DIR
rm -rf $DIR springpython_$2.orig.tar.gz
