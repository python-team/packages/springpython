=========
 coily
=========

----------------------------------------------
command-line management tool for Spring Python
----------------------------------------------

:Author: Greg Turnquist <gturnquist at vmware dotcom>
:Date:   2010-11-03
:Copyright: Apache Software License
:Version: 1.2.0
:Manual section: 1
:Manual group: Spring Python Documentation

SYNOPSIS
========

coily command

DESCRIPTION
===========

Coily is the command-line tool that utilizes the plugin system. It is similar
to grails command-line tool, in that through a series of installed plugins, you
are able to do many tasks, including build skeleton apps that you can later
flesh out. If you look at the details of this app, you will find a
sophisticated, command driven tool to built to manage plugins. The real power
is in the plugins themselves.

For information about Spring Python, it can be found at
http://www.springpython.org/

OPTIONS
=======

--help                            Displays a brief summary with all options.
--list-installed-plugins          List the plugins currently installed in this account.
--list-available-plugins          List the plugins available for installation.
--install-plugin <name>           Install the named plugin.
--uninstall-plugin <name>         Uninstall the named plugin by deleting its entry from ~/.springpython.
--reinstall-plugin <name>         Uninstall then install the plugin.

SEE ALSO
========

The full documentation for coily and Spring Python is maintained as a HTML
manual available at http://static.springsource.org/spring-python/1.2.x/sphinx/html/.
